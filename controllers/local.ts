import {LocalModel, ILocal} from '../models/local';

export class Local {
    private _local: ILocal;

    static ListAll():Promise<Array<Local>>{
        return new Promise((resolve, reject) => {
            LocalModel.find().exec((err, locais) => {
                if (err) {
                    reject(err);
                }else{
                    var ListaLocais = new Array<Local>();
                    
                    locais.forEach((ALocal, i) => {
                        ListaLocais.push(new Local(ALocal));
                    });

                    resolve(ListaLocais);
                }
            });
        });
    }

    constructor(ALocal?: ILocal) {  
        this._local = ALocal;
    }

    public get nome() : String {
        return this._local.nome;
    }
    
    public saveLocal():Promise<ILocal>{
        return new Promise( (resolve, reject) => {
            LocalModel.create(this._local,(err, local) => {
                err ? reject(err) : resolve(local);            
            });
        });
    }
}
