import {UserModel, IUser} from '../models/usuarios';

export module Usuarios{
    /**
     * Usuario
     */
    export class Usuario {
        private _usuario: IUser;


        static findOrCreateUser(alogin: string, aprovider: string): Promise<Usuario>{
            return new Promise <Usuario>( (resolve, reject) => {
                UserModel.findOne({provider: aprovider, login: alogin})
                .exec()
                .then((user) => {
                    if (user) {
                        resolve(new Usuario(user));    
                    }else{
                        UserModel.create({login: alogin, provider: aprovider}, (err, newUser) => {
                            err ? reject(err) : resolve(new Usuario(newUser));
                        });                
                    }
                });
            });
        }

        constructor(auser: IUser) {  
            this._usuario = auser;
        }
    }
}