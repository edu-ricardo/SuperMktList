# Super MktList
Aplicação de lista de compras pro supermercado.

## Requisitos

* RF1. Cadastrar/Logar Usuário
* RF2. Incluir/Pesquisar produtos (Produtos são compartilhados )
* RF3. Incluir Lojas (Mercados)
* RF4. Cadastrar preço para um produto (gerar um preço médio)
* RF5. Cadastrar Lista de compras calcular um valor esperado de acordo com o preço médio
  * RNF1. Criar meio de ler a etiqueta de preço com a camera do celular para calcular o preço correto quando comprado
  * RNF2. Criar na lista um teto de valor

## Tecnologias

### Server
  1. MongoDB - Banco de Dados
  2. NodeJS - Express/Mongoose
  3. TypeScript
  
### Client
  1. Cordova
  2. Angular
