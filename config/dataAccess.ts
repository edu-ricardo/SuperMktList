/// <reference path="../typings/tsd.d.ts" />

import config = require('./config');
import * as Mongoose from 'mongoose' ;

class DataAccess{
    static Instancia: any;
    static Conexao: Mongoose.Connection;

    static connect(){
        if(this.Instancia) return this.Instancia;

        this.Conexao = Mongoose.connection;
        this.Conexao.once('open', () => {
            console.log('Conectado ao Mongodb em '+ config.Database.DB_CONNECTION_STRING);
        });

        this.Instancia = Mongoose.connect(config.Database.DB_CONNECTION_STRING);
        return this.Instancia;
    }
}

DataAccess.connect();

export = DataAccess;
