/// <reference path="../typings/tsd.d.ts" />
import * as mongoose from 'mongoose';
import Schema = mongoose.Schema;
import Types = mongoose.Types;
import ObjectId = Types.ObjectId;

export module Preco{   

    export interface IPreco extends mongoose.Document  {
        local: ObjectId;
        especificacao: string;
        precos: [{
            data: Date,
            valor: Number
        }];
        precoAtual: number;
        produto: ObjectId;
    }
}