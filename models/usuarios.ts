import DataAccess = require('../config/dataAccess');
import mongoose = require('mongoose');
var mongo = DataAccess.Instancia;

export interface IUser extends mongoose.Document {
  provider: string;
  displayName: string;
  emails: string;
  photos: any;
  show: boolean;
  created: Date;
  updated: Date;
}

/**
 * MongooseSchema
 * @type {"mongoose".Schema}
 * @private
 */
var _schema: mongoose.Schema = new mongo.Schema({
    provider: {
      type: String,
      require: true
    },
    displayName: {
      type: String
    },
    emails: {
      type: String
    },
    photos: {
      type: mongoose.Schema.Types.Mixed
    },
    show: Boolean,
    created: {
      type: Date,
      default: Date.now
    },
    updated: {
      type: Date,
      default: Date.now
    }
  })
  .pre('save', function(next) {
    this.updated = new Date();
    next();
  });

/**
 * Mongoose.Model
 * @type {Model<IUser>}
 * @private
 */
export var UserModel = mongoose.model < IUser > ('User', _schema);