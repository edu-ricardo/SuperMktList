/// <reference path="../typings/tsd.d.ts" />

import DataAccess = require('../config/dataAccess');
import * as Preco from './preco-model';
import * as mongoose from 'mongoose';


import Schema = mongoose.Schema;
import Types = mongoose.Types;
import ObjectId = Types.ObjectId;

var conexao = DataAccess.Conexao;
var mongo = DataAccess.Instancia;

class PrecoSchema {
    static get schema (){
        var schema = mongo.Schema({
            local: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'local'
            },
            especificacao: String,
            precos: [{
                data: Date,
                valor: Number
            }],
            precoAtual: Number,
            produto: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'produto'                
            }
        });
        return schema;
    }
}


var schema = conexao.model<Preco.Preco.IPreco>("Preco", PrecoSchema.schema);
export = schema;