import * as mongoose from 'mongoose';

export module Produto{
    export interface IProduto extends mongoose.Document  {
        nome: string;
        unVenda: string;
        precos: Array<Object>;
        precoMedio: number;
    }
}