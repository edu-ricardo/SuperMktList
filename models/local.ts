//import DataAccess = require('../config/dataAccess');
import mongoose = require('mongoose');
//var mongo = DataAccess.Instancia;

export interface ILocal extends mongoose.Document  {
    nome: String;
    endereco: String;
    site: String;
    dateCreated: Date;
    dateModified: Date;
}

/**
 * MongooseSchema
 * @type {"mongoose".Schema}
 * @private
 */
var _schema: mongoose.Schema = new mongoose.Schema({
    nome: String,
    endereco: String,
    site: String,
    dateCreated: {
        type: Date,
        default: Date.now()
    },
    dateModified: {
        type: Date,
        default: Date.now()
    }
  })
  .pre('save', function(next) {
    this.dateModified = new Date();
    next();
  });
/**
 * Mongoose.Model
 * @type {Model<ILocal>}
 * @private
 */
export var LocalModel = mongoose.model<ILocal>('Local', _schema);