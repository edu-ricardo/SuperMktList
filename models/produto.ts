/// <reference path="../typings/tsd.d.ts" />

import DataAccess = require('../config/dataAccess');
import * as prodmodel from './produto-model';
import * as mongoose from 'mongoose';

var conexao = DataAccess.Conexao;
var mongo = DataAccess.Instancia;

class ProdutoSchema {
    static get schema (){
        var schema = mongo.Schema({
            nome: {
                type: String,
                required: true
            },
            unVenda: {
                type: String,
                default: 'unidade'
            },
            precos: [],
            precoMedio:{
                type: Number
            }
        }); 

        return schema;
    }
}


var schema = conexao.model<prodmodel.Produto.IProduto>("Produto", ProdutoSchema.schema);

export = schema;