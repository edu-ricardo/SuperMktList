/// <reference path="../typings/tsd.d.ts" />
import * as express from 'express'; 
const router = express.Router();
import ProdutoSchema = require('../models/produto');

router.get('/produto', function (req, res) {
    ProdutoSchema.find().exec((err, produtos) => {
        res.json(produtos).end();
    });;    
});

router.get('/produto/:id', function (req, res) {
    var id = req.params.id;
    ProdutoSchema.find({_id: id}).exec((err, produtos) => {
        res.json(produtos).end();
    });;    
});

router.post('/produto', function (req, res) {
    ProdutoSchema.create(req.body,function(err, produto){
        if(err){
            console.error(err);
            res.json(err).status(500).end();
        }else{
            res.json(produto).status(201).end();
        }

    });
});

router.put('/produto/:id', function (req, res) {
    var id = req.params.id;

    ProdutoSchema.findOneAndUpdate({_id: id},req.body,function(err, produto){
        if(err){
            console.error(err);
            res.json(err).status(500).end();
        }else{
            res.json(produto).status(201).end();
        }        
    });
});

router.delete('/produto/:id', function (req, res) {
    var id = req.params.id;
    ProdutoSchema.findOneAndRemove({_id: id}, function(err, produto){
        if(err){
            console.error(err);
            res.json(err).status(500).end();
        }else{
            res.json(produto).status(200).end();
        }
    });
});

export = router;