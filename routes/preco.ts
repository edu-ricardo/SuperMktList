/// <reference path="../typings/tsd.d.ts" />
import * as express from 'express'; 
const router = express.Router();
import PrecoSchema = require('../models/preco');

router.get('/preco', function (req, res) {
    PrecoSchema.find().exec((err, preco) => {
        if(err){
            console.error(err);
            res.json(err).status(500).end();
        }else{
            res.json(preco).end();
        }
    });
});

router.get('/preco/:id', function (req, res) {
    
});

router.post('/preco', function (req, res) {
    
});

router.put('/preco/:id', function (req, res) {
    
});

router.delete('/preco/:id', function (req, res) {
    
});

export = router;