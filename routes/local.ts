/// <reference path="../typings/tsd.d.ts" />
import * as express from 'express'; 
import {Local} from '../controllers/local';
import {ILocal} from '../models/local';
const router = express.Router();


router.post('/local', function (req, res) {
    console.log('I\'m Here');
    var __local = req.body;
    console.log(__local);
    var local = new Local(__local);

    local.saveLocal()
    .then((newLocal) => {
        res.json(newLocal).status(201).end();
    })
    .catch((err) => {
        console.error(err);
        res.json(err).status(500).end();
    }); 
});

router.get('/local/:id', function (req, res) {
    
});

router.get('/local', function (req, res) {
    Local.ListAll()
    .catch((err) => {
        console.error(err);
        res.json(err).status(500).end();
    })
    .then((Locais) => {
        res.json(Locais).status(200).end();
    });
});

router.put('/local/:id', function (req, res) {
    
});

router.delete('/local/:id', function (req, res) {
    
});

export = router;