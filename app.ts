/// <reference path="typings/tsd.d.ts" />

import * as express from 'express';
import * as bp from 'body-parser';
import DataAccess = require('./config/dataAccess');

const app = express();
const port = process.env.PORT || 8080;

app.use(bp.json());

import routerProd = require('./routes/produtos');
import routerPreco = require('./routes/preco');
import routerLocal = require('./routes/local');
app.use('/api', routerProd);
app.use('/api', routerPreco);
app.use('/api', routerLocal);

app.listen(port, function(){
    console.log('Listen at '+ port);
});
